#!/usr/bin/env python3

from time import mktime
import datetime
import re
# from scholar import scholar

import feedparser
from crossref.restful import Works, Etiquette
import yaml
import click
from tqdm import tqdm

from ._version import __version__  # NOQA

__all__ = ['__version__', 'update']

# Arxiv api helper functions
_id_from_url = re.compile('https?://arxiv.org/abs/(?P<id>.*?)(?:v[0-9]+)?$')
author_feed = 'https://arxiv.org/a/{}.atom'.format


def id_feed(arxiv_ids):
    return (
        f'https://export.arxiv.org/api/query?id_list={",".join(arxiv_ids)}'
        f'&max_results={len(arxiv_ids)}'
    )


def id_from_url(url):
    return _id_from_url.match(url).group('id')


# Business logic
def parse_crossref_output(data):
    for pub_type in 'published-online', 'published-print':
        try:
            date = data[pub_type]['date-parts'][0]
        except KeyError:
            continue
    # If a journal provides an incomplete date.
    date = datetime.date(*(date + [1] * (3 - len(date))))
    try:
        page = data['article-number']
    except KeyError:
        if 'page' in data:
            page = data['page'].split('-')[0]
        else:
            page = None
    result = dict(
        journal=data['short-container-title'][0],
        vol=data.get('volume', ''),
        page=page,
        published=date,
        journal_url=data['URL'],
    )
    return result


def feed_entries(feed):
    updates = feedparser.parse(feed)

    if not updates['bozo']:  # We succeeded
        result = updates['entries']
    else:
        result = []
    return map(preprint_to_dict, result)


def preprint_to_dict(preprint):
    result = {
        'id': id_from_url(preprint['id']),
        'title': ' '.join(preprint['title'].replace('\n', ' ').split()),
        'authors': [a['name'] for a in preprint['authors']],
        'abstract': ' '.join(preprint['summary'].replace('\n', ' ').split()),
        'on_arxiv': datetime.date.fromtimestamp(
            mktime(preprint['published_parsed'])
        ),
    }
    if 'arxiv_doi' in preprint:
        result['doi'] = preprint['arxiv_doi']
    return result


@click.command()
@click.argument(
    'output',
    nargs=1,
    type=click.Path(dir_okay=False, writable=True, resolve_path=True)
)
@click.option(
    '--author_id', '-a', multiple=True,
    help='arxiv ids of the authors'
)
@click.option(
    '--preprint_id', '-p', multiple=True,
    help='extra preprint identifiers'
)
@click.option(
    '--update/--no-update', '-u', default=False,
    help="Check for updates in existing data"
)
@click.option(
    '--overwrite/--no-overwrite', '-f', default=False,
    help="Prefer data fetched from arxiv to the one stored locally."
)
@click.option('--silent/--no-silent', '-s', default=False)
@click.option('--email', help='Email for faster DOI updates')
def update(
    output,
    update,
    author_id,
    preprint_id,
    overwrite,
    silent,
    email
):
    try:
        with open(output, 'r', encoding='utf8') as f:
            data = yaml.load(f, Loader=yaml.SafeLoader)
    except FileNotFoundError:
        data = []

    feeds = [author_feed(author) for author in author_id]
    preprint_id = set(preprint_id)
    known_ids = set(i['id'] for i in data)
    if update:
        preprint_id |= known_ids
    else:
        preprint_id -= known_ids

    feeds.append(id_feed(set(preprint_id)))
    if not silent:
        feeds = tqdm(feeds, desc="Fetching feeds")
    entries = {
        preprint['id']: preprint
        for feed in feeds
        for preprint in feed_entries(feed)
    }
    updates = False
    for i, item in enumerate(data):
        new = entries.pop(item['id'], {})
        data[i] = {**item, **new} if overwrite else {**new, **item}
        updates = updates or item != data[i]
    updates = updates or bool(entries)
    data.extend(entries.values())

    # Fetch all updated DOIs:
    to_update_doi = [i for i in data if 'doi' in i and 'published' not in i]
    works = Works(etiquette=Etiquette(
        'publist', __version__, contact_email=(email or 'anonymous')
    ))
    updates = updates or to_update_doi
    if not silent:
        to_update_doi = tqdm(to_update_doi, desc='Updating DOI')
    for item in to_update_doi:
        try:
            item.update(parse_crossref_output(works.doi(item['doi'])))
        except Exception:
            print(f'Failed to parse crossref output for {item["id"]}')

    if updates:
        with open(output, 'w', encoding='utf8') as f:
            yaml.dump(data, f, default_flow_style=False, allow_unicode=True)

    if not silent:
        print('Updated some entries') if updates else print('All up to date')
