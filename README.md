# Publication list

A script for maintaining a publication list

## Installation

```
pip install git+https://gitlab.kwant-project.org/anton-akhmerov/publist.git
```

## Usage

```
Usage: publist [OPTIONS] OUTPUT

Options:
  -a, --author_id TEXT            arxiv ids of the authors
  -p, --preprint_id TEXT          extra preprint identifiers
  -u, --update / --no-update      Check for updates in existing data
  -f, --overwrite / --no-overwrite
                                  Prefer data fetched from arxiv to the one
                                  stored locally.
  -s, --silent / --no-silent
  --email TEXT                    Email for faster DOI updates
  --help                          Show this message and exit.
```