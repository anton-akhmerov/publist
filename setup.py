#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
import sys


if sys.version_info < (3, 6):
    print('publist requires Python 3.6 or above.')
    sys.exit(1)


# Loads version.py module without importing the whole package.
def get_version_and_cmdclass(package_name):
    import os
    from importlib.util import module_from_spec, spec_from_file_location
    spec = spec_from_file_location('version',
                                   os.path.join(package_name, '_version.py'))
    module = module_from_spec(spec)
    spec.loader.exec_module(module)
    return module.__version__, module.cmdclass


version, cmdclass = get_version_and_cmdclass('publist')


install_requires = [
    'feedparser',
    'crossrefapi',
    'pyyaml',
    'Click',
    'tqdm',
]


setup(
    name='publist',
    version=version,
    cmdclass=cmdclass,
    description='Create and maintain a publication list from arXiv',
    url='https://gitlab.kwant-project.org/anton-akhmerov/publist',
    author='Anton Akhmerov',
    license='BSD 2-clause',
    classifiers=[
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: BSD License',
        'Intended Audience :: Science/Research',
        'Programming Language :: Python :: 3.6',
    ],
    packages=find_packages('.'),
    install_requires=install_requires,
    entry_points={
        'console_scripts': [
            'publist = publist.publist:update',
        ],
    }
)
